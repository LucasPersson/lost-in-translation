# Lost in translation


[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://lucaspersson.gitlab.io/lost-in-translation/)

Sign language translator made with react.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project was created for an assignment to learn more about React.


## Install

This project uses node and npm. Go check them out if you don't have them locally installed.


Open a terminal or powershell window and run:

```sh
npm install
```

## Usage

Open a terminal or powershell window and run:

```sh
npm run dev
```

Further instructions will appear in your console. Leave the window open while in use.

## Maintainers

[Lucas Persson (@LucasPersson)](https://gitlab.com/LucasPersson)
[Alexander Grönberg (@DrAvokad)](https://gitlab.com/DrAvokad)

## Acknowledgements

Special thanks! For @sumodevelopment contributing with educational videos.

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Lucas Persson & Alexander Grönberg

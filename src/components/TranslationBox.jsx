import AImg from '../assets/individial_signs/a.png'
import BImg from '../assets/individial_signs/b.png'
import CImg from '../assets/individial_signs/c.png'
import DImg from '../assets/individial_signs/d.png'
import EImg from '../assets/individial_signs/e.png'
import FImg from '../assets/individial_signs/f.png'
import GImg from '../assets/individial_signs/g.png'
import HImg from '../assets/individial_signs/h.png'
import IImg from '../assets/individial_signs/i.png'
import JImg from '../assets/individial_signs/j.png'
import KImg from '../assets/individial_signs/k.png'
import LImg from '../assets/individial_signs/l.png'
import MImg from '../assets/individial_signs/m.png'
import NImg from '../assets/individial_signs/n.png'
import OImg from '../assets/individial_signs/o.png'
import PImg from '../assets/individial_signs/p.png'
import QImg from '../assets/individial_signs/q.png'
import RImg from '../assets/individial_signs/r.png'
import SImg from '../assets/individial_signs/s.png'
import TImg from '../assets/individial_signs/t.png'
import UImg from '../assets/individial_signs/u.png'
import VImg from '../assets/individial_signs/v.png'
import WImg from '../assets/individial_signs/w.png'
import XImg from '../assets/individial_signs/x.png'
import YImg from '../assets/individial_signs/y.png'
import ZImg from '../assets/individial_signs/z.png'

const TranslationBox = (props) => {
    const { textInput } = props

    //Alphabet to check if symbol to translate is an alphabetic character
    const alphabet = "abcdefghijklmnopqrstuvwxyz"

    //Takes each character from text input, checks if it is an alphabetic character and if it is
    //renders the corresponding image
    const translation = textInput.map((letter, index) => {
        if (alphabet.includes(letter.toLowerCase())) {
            if(letter.toLowerCase()==="a"){
                return <img key={index} src={AImg}></img>
            }else if(letter.toLowerCase()==="b"){
                return <img key={index} src={BImg}></img>
            }else if(letter.toLowerCase()==="c"){
                return <img key={index} src={CImg}></img>
            }else if(letter.toLowerCase()==="d"){
                return <img key={index} src={DImg}></img>
            }else if(letter.toLowerCase()==="e"){
                return <img key={index} src={EImg}></img>
            }else if(letter.toLowerCase()==="f"){
                return <img key={index} src={FImg}></img>
            }else if(letter.toLowerCase()==="g"){
                return <img key={index} src={GImg}></img>
            }else if(letter.toLowerCase()==="h"){
                return <img key={index} src={HImg}></img>
            }else if(letter.toLowerCase()==="i"){
                return <img key={index} src={IImg}></img>
            }else if(letter.toLowerCase()==="j"){
                return <img key={index} src={JImg}></img>
            }else if(letter.toLowerCase()==="k"){
                return <img key={index} src={KImg}></img>
            }else if(letter.toLowerCase()==="l"){
                return <img key={index} src={LImg}></img>
            }else if(letter.toLowerCase()==="m"){
                return <img key={index} src={MImg}></img>
            }else if(letter.toLowerCase()==="n"){
                return <img key={index} src={NImg}></img>
            }else if(letter.toLowerCase()==="o"){
                return <img key={index} src={OImg}></img>
            }else if(letter.toLowerCase()==="p"){
                return <img key={index} src={PImg}></img>
            }else if(letter.toLowerCase()==="q"){
                return <img key={index} src={QImg}></img>
            }else if(letter.toLowerCase()==="r"){
                return <img key={index} src={RImg}></img>
            }else if(letter.toLowerCase()==="s"){
                return <img key={index} src={SImg}></img>
            }else if(letter.toLowerCase()==="t"){
                return <img key={index} src={TImg}></img>
            }else if(letter.toLowerCase()==="u"){
                return <img key={index} src={UImg}></img>
            }else if(letter.toLowerCase()==="v"){
                return <img key={index} src={VImg}></img>
            }else if(letter.toLowerCase()==="w"){
                return <img key={index} src={WImg}></img>
            }else if(letter.toLowerCase()==="x"){
                return <img key={index} src={XImg}></img>
            }else if(letter.toLowerCase()==="y"){
                return <img key={index} src={YImg}></img>
            }else if(letter.toLowerCase()==="z"){
                return <img key={index} src={ZImg}></img>
            }
            // return (
            //     <img key={index} src={"/src/assets/individial_signs/" + letter + ".png"}></img>
            // )
        }
    })

    return (
        <div>
            {translation}
        </div>
    )

}

export default TranslationBox


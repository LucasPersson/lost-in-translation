import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../API/user'
import { useUser } from '../state/UserContext'
import { addLocalStorage } from '../utils/storage'
import { useNavigate } from 'react-router-dom'
import { STORAGE_USER_KEY } from '../const/storageKeys'

//Conditions for the input in form
const usernameConfig = {
    required: true,
    minLength: 3
}


const LoginForm = () => {
    //hooks

    // assign to react hook form use form to handle data from the form
    const { register, handleSubmit, formState: {errors}} = useForm()

    //hook to get user and setUser from userContext hook
    const { user, setUser } = useUser()
    //get navigate from react router dom
    const navigate = useNavigate()

    //Local state
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError] = useState(null)

    // side effects 
    //check if user exist to then navigate to translate page instead 
    useEffect(() => {
        if (user !== null) {
            navigate('/translate')
        }
    }, [ user, navigate ])  

    // when form is submitted send username to login user for validation if already exist or to create
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [ error, userResponse ] = await loginUser(username)
        if (error !== null) {
            setApiError(error)

        }
        // if login user got a user back or created a user set it in localStorage
        if (userResponse !== null) {
            addLocalStorage(STORAGE_USER_KEY, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }
    
    // error messages for the form requirements
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }
        if (errors.username.type === 'required') {
            return <span>Username is required!</span>
        }
        if (errors.username.type === 'minLength') {
            return <span>Username is to short!</span>
        }

    })()


    return (
        <>
            <form className='loginForm' onSubmit={ handleSubmit(onSubmit) }>
                <fieldset>    
                <input type="text" placeholder="What's your name?" {...register('username', usernameConfig)} />
                {errorMessage}
                <button type="submit" disabled={ loading }>Login</button>
                </fieldset>
                { loading && <span>Loading</span> }
                { apiError && <span>{apiError}</span> }
            </form>
        
        
        
        </>


    )
}

export default LoginForm

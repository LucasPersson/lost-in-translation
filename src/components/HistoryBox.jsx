import TranslationItem from "./TranslationItem"
import { useState } from "react";

const HistoryBox = ({ translations }) => {
    const [translationsHistory, setTranslations] = translations



    // map over the translations array in user to print each translation string
    // as long as clear variable is false in each translation object
    const translationList = translations.map((translation, index) => {
        //if (!translation.cleared) {
            return <TranslationItem key={index + '-' + translation.text} translation={translation.text} />
        //}
    })

    return (
        <div>
            <p>Your translations</p>
            <ul className="translationUl">
                {translationList}
            </ul>
        </div>
    )
}
export default HistoryBox

import { useUser } from '../state/UserContext'
import { addLocalStorage } from '../utils/storage'
import { STORAGE_USER_KEY } from '../const/storageKeys'
import TranslationBox from "../components/TranslationBox"
import { useState } from 'react'
import { updateTranslationRecords } from '../API/user'

const SearchField = () => {

    //User context
    const { user, setUser } = useUser()
    //Word submitted for translation
    const [wordToTranslate, setTranslate] = useState([])

    //Input of textfield - updates on change
    let currentInput = ""

    //Takes the current input variable and sends it to the translation component as well as
    //updating the user context, local storage and the user API
    const translateInput = async () => {
        let newUser = user
        let newTranslations = newUser.translations
        newTranslations = [
            {
                cleared: false,
                text: currentInput
            },
            ...newTranslations]
        newUser = { ...user, translations: newTranslations }
        await updateTranslationRecords(user.id, newTranslations)
        setTranslate(currentInput.split(''))
        setUser(newUser)
        addLocalStorage(STORAGE_USER_KEY, newUser)

    }

    //Handler for change in input
    const handleInputChange = (event) => {
        currentInput = event.target.value
    }

    return (
        <div>
            <input type="text" onChange={handleInputChange} placeholder="What would you like to translate?" />
            <button type="button" onClick={translateInput}>Translate</button>
            <TranslationBox textInput={wordToTranslate} />
        </div>
    )
}


export default SearchField;
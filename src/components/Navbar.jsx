import { NavLink } from "react-router-dom"
import { useUser } from "../state/UserContext"

const Navbar = () => {

    const { user } = useUser()

    return (
        <nav>
            <div className="navDivTitle">
                <h1>Lost in Translation</h1>
            </div>
            

            { user !== null &&

            <div className="navDivLoggedIn">
                <p className="navLink">
                    <NavLink to='/translate'>Translate</NavLink>
                </p>
                <p className="navLink">
                    <NavLink to='/profilePage'>Profile</NavLink>
                </p>
            </div>
            }
        </nav>
    )
}
export default Navbar
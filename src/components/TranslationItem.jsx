
//takes the translation and adds it to an li used by the history box
const TranslationItem = ({ translation }) => {
    return <li className="translationLi">{translation}</li>
}
export default TranslationItem
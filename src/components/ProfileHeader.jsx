const ProfileHeader = ({ username }) => {
    return (
        <header>
            <h1>Hi, { username }!</h1>
        </header>
    )
}
export default ProfileHeader
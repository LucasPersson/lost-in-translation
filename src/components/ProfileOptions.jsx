import { Link } from "react-router-dom"
import { useUser } from '../state/UserContext'
import { addLocalStorage } from '../utils/storage'
import { STORAGE_USER_KEY } from '../const/storageKeys'
import { updateTranslationRecords } from '../API/user'
import { useEffect, useState } from "react"
import HistoryBox from "../components/HistoryBox";

const ProfileOptions = ({ logoutUser }) => {

    const { user, setUser } = useUser()
    const [translationList, setList] = useState([])

    useEffect(() => {
        let i = 0;
        let newTransArray = []
        for(let translation of user.translations){
            i++;
            if(i <= 10 && !translation.cleared){
                newTransArray.push(translation)
            }
        }
        setList(newTransArray)
    },[]);

    //logout user calls for function in profile page
    const logout = () => {
        if (window.confirm('Are you Sure?')) {
            logoutUser()
        }
    }

    //Sets the clear boolean of each translationobject of the logged in user to true
    //and then updates this change in user context, local storage and the user API
    const clearTranslations = async () => {
        let newUser = user
        let newTranslations = newUser.translations
        for (let item of newTranslations) {
            item.cleared = true
            console.log(item)
        }
        newUser = { ...user, translations: newTranslations }
        setList([])
        await updateTranslationRecords(user.id, newTranslations)
        setUser(newUser)
        addLocalStorage(STORAGE_USER_KEY, newUser)
    }

    return (
        <div>
            <button onClick={clearTranslations}>Clear Translations</button>
            <button onClick={ logout }>Log out</button>
            <HistoryBox translations={translationList} />
        </div>
    )
}
export default ProfileOptions

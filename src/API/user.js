//gets all users from api
import { BASE_URL, createHeaders  } from ".";

export async function updateTranslationRecords(userId, userTranslations) {
    try {
        const config = {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                id: userId,
                translations: userTranslations 
            })
        };

        const response = await fetch(
            `${BASE_URL}/translations/${userId}`, config
        );

        await response.json()
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not update translation records')
                }
                return response.json()
            })
            .then(updatedUser => {

            })
    } catch (error) {
        return [error.message, null];
    }
}



// gets specific user to se if it exist in api
const checkForUser = async (username) => {
    try {
     
        const response = await fetch( `${BASE_URL}/translations?username=${username}`)
        if(!response.ok) {
            throw new Error('could not complete request.')
        }
        const data = await response.json()
        return [null, data]
    } 
    catch (error) {
        return [ error.message, []]
    }
}

// creates a user 
const createUser = async (username) => {
    try {
        const response = await fetch( `${BASE_URL}/translations`, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok) {
            throw new Error(`could not create user with ${username}.`)
        }
        const data = await response.json()
        return [null, data]
    } 
    catch (error) {
        return [ error.message, []]
    }
}


// checks in user exist in check for user if user exist returns user else createUser
export const loginUser = async (username) => {
    const [ checkError, user] = await checkForUser(username)

    if (checkError !== null) {
        return [ checkError, null ]
    }

    if (user.length > 0) {
        return [null, user.pop()]
    }

    return await createUser(username)
}
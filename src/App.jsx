import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import StartPage from "./views/StartPage.jsx"
import TranslationPage from "./views/TranslationPage"
import ProfilePage from './views/ProfilePage'
import Navbar from './components/Navbar'

function App() {
  const basename =
  process.env.NODE_ENV === 'production'
    ? '/lost-in-translation/'
    : '/'


  return (
    <div>
      <BrowserRouter basename={basename} >
      <Navbar />
        <Routes>
          <Route path="/" element={<StartPage/>}/>
          <Route path="/translate" element={<TranslationPage/>}/>
          <Route path="/profilePage" element={<ProfilePage/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App

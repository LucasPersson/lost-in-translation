import { createContext, useContext, useState } from "react";
import { STORAGE_USER_KEY } from "../const/storageKeys";
import { readLocalStorage } from "../utils/storage";

//context
const UserContext = createContext()

//hook to return user context from the userProvider
export const useUser = () => {
    return useContext(UserContext) // user, setUser
}
//provider
const UserProvider = (props) => {

    const [ user, setUser ] = useState( readLocalStorage(STORAGE_USER_KEY))
    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={ state }>
            {props.children}
        </UserContext.Provider>

    )
}
export default UserProvider
import withAuth from "../hoc/withAuth";
import SearchField from "../components/SearchField";

const TranslationPage = () => {

    return (
        <div className="App">
            <h1> Translation Page</h1>
            <SearchField/>
        </div>
    );
};

export default withAuth(TranslationPage)
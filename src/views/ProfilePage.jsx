import ProfileHeader from "../components/ProfileHeader";
import ProfileOptions from "../components/ProfileOptions";
import { STORAGE_USER_KEY } from "../const/storageKeys";
import withAuth from "../hoc/withAuth";
import { useUser } from "../state/UserContext";
import { deleteLocalStorage } from "../utils/storage";

const ProfilePage = () => {
    // get user and set user from hook
    const { user, setUser } = useUser()
    //logout and clear storage and user
    const logoutUser = () => {
        deleteLocalStorage(STORAGE_USER_KEY)
        setUser(null)
    }

    return (
        <div className="App">
            <>
            <ProfileHeader username={user.username} />
            <ProfileOptions logoutUser={ logoutUser } />
            </>
        </div>
    )
}

export default withAuth(ProfilePage)
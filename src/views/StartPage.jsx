import LoginForm from "../components/LoginBox"



const StartPage = () => {
    return (
        <>
        <div className="App">
            <h1>Lost in Translation</h1>
            <h3>Get started</h3>
        <LoginForm/>
        </div>
        </>
    )
}
export default StartPage
// function to handle set item to localStorage in this case our user
export const addLocalStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value))
}
//function to check localStorage 
export const readLocalStorage = key => {

    const data = localStorage.getItem(key)
    if (data) {
        return JSON.parse(data)
    }

    return null 
}
// remove from localStorage
export const deleteLocalStorage = key => {
    localStorage.removeItem(key)
}
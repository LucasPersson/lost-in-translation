import { Navigate } from "react-router-dom"
import { useUser } from "../state/UserContext"

//higher order function to handle validation so a user that is not logged in cant access pages besides login page
const withAuth = Component => props => {
    const { user } = useUser()
    if (user !== null) {
        return <Component {...props} />
    } else {
        return <Navigate to='/' />
    }
}
export default withAuth